import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-outros',
  templateUrl: './outros.component.html',
  styleUrls: ['./outros.component.css']
})
export class OutrosComponent implements OnInit {

  data = [];

  constructor() { 

    this.getData();

  }

  getData(){
    fetch('https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/5UXWX7C5*BA?format=json&modelyear=2011')
      .then(r => r.json())
      .then(json => {
        console.log(json.Results)
        this.data = json.Results;
      });
  }

  ngOnInit() {
  }

}
