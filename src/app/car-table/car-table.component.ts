import { Component, OnInit } from '@angular/core';
import { CarsService }from '../cars.service';

@Component({
  selector: 'app-car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.css']
})
export class CarTableComponent implements OnInit {

  cars = [];

  constructor() { 
    this.cars = CarsService.getCars();
  }

  ngOnInit() {
  }

}
