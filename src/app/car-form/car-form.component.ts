import { Component, OnInit, NgModule } from '@angular/core';
import { CarsService }from '../cars.service';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {

  marca = '';
  modelo = '';
  ano = '';
  erro = false;

  constructor() { }

  ngOnInit() {
  }

  clearForm(){
    this.marca = '';
    this.modelo = '';
    this.ano = '';
    this.erro = false;
  }
  
  registerCar() {
    let {
      marca,
      modelo,
      ano,
    } = this;

    if(marca && modelo && ano) {
      CarsService.setCar({
        marca,
        modelo,
        ano,
      });

      this.clearForm();
      alert('Carro cadastrado com sucesso!')
    } else {
      this.erro = true;
    }
    return false;
 }
}
