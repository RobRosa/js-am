import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  static getCars(){
    return JSON.parse(window.localStorage.getItem('carsList') || "[]");
  }

  static setCar(objCar){
    let carsList = window.localStorage.getItem('carsList');
    if (!carsList) {
      window.localStorage.setItem('carsList', JSON.stringify([objCar]));
    } else {
      carsList = JSON.parse(carsList);
      carsList.push(objCar);
      window.localStorage.setItem('carsList', JSON.stringify(carsList));
    }
  }
}
