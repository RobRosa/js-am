import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CarTableComponent } from './car-table/car-table.component';
import { HomeComponent } from './home/home.component';
import { CarFormComponent } from './car-form/car-form.component';
import { FormsModule } from '@angular/forms';
import { OutrosComponent } from './outros/outros.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'cars', component: CarTableComponent },
  { path: 'cadastrar', component: CarFormComponent },
  { path: 'outros', component: OutrosComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    CarTableComponent,
    HomeComponent,
    CarFormComponent,
    OutrosComponent
  ],
  imports: [
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
