import { Component, OnInit } from '@angular/core';
import { CarTableComponent } from '../car-table/car-table.component';
import { CarsService }from '../cars.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data = new Date();

  constructor() { 
    window.setInterval(() => {this.data = new Date()}, 1000);
  }

  ngOnInit() {
    console.log(CarsService.getCars());
  }

}
